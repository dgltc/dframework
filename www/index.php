<?php
include_once('../config/config.ini.php');
include_once(PATH_CONFIG.'/url.ini.php');
include_once(PATH_LIBS.'/urlparser.class.php');
include_once(PATH_LIBS.'/dispatcher.class.php');

/*errores*/
if(ENV=='DEV') error_reporting (E_ALL);
$url        = new url();
$dispatcher = new dispatcher();

if(ENV=='DEV' && unserialize(SCRIPT_PARAMS)['debug']==1 ) 
{
    include_once(PATH_LIBS.'/debug.php');
}

