<?php
define(WEB_STATUS,      'LANDING'); // LANDING/EMPTY
define(BASE_DOMAIN,     'domain.com');
define(DEV_BASE_DOMAIN, 'dev.domain.com');
define(BASE_URL,        'http://www.domain.com');
define(BASE_URL_MOBILE, 'http://m.domain.com');
define(FAVICON,         'http://www.domain.com/favicon.ico');
define(SITE_NAME,       'Mi Site Name');
define(ENV,             $setEnv = (!preg_match("/DEV_BASE_DOMAIN/", $_SERVER["HTTP_HOST"]))?'DEV':'PROD');
define(PATH_CONFIG,     '../config');
define(PATH_LIBS,       '../libs');
define(PATH_CONTROLLER, '../controller');
define(PATH_MODEL,      '../model');
define(PATH_TEMPLATES,  '../templates');
define(PATH_WWW,        '..');
define(PATH_CSS,        '../static/css');
define(PATH_JS,         '../static/js');
define(PATH_IMAGES,     '../static/images');
define(PATH_FONTS,      '../static/fonts');
define(DATABASE_NAME,   'ddbb_name');
define(DATABASE_HOST,   'localhost');
define(DATABASE_PORT,   '3306');
define(DATABASE_PASS,   'password');
define(DATABASE_USER,   'db_user');


?>