<?php
$url_config['home']['url_param']    = '';
$url_config['home']['template']     = 'home.tpl.php';
$url_config['home']['controllers']  = array('home.controller.php');
$url_config['home']['db']           = true;


$url_config['item']['url_param']    = 'casa';
$url_config['item']['template']     = 'item.tpl.php';
$url_config['item']['controllers']  = array('item.controller.php', 'db.controller.php');
$url_config['item']['libs']         = array('db.lib.php');

$url_config['find']['url_param']    = 'encontrar';
$url_config['find']['template']     = 'find.tpl.php';
$url_config['find']['controllers']  = array('find.controller.php', 'db.controller.php');
$url_config['find']['libs']         = array('pdoCacheDb.lib.php');


$url_config['action']['url_param']    = 'action';
$url_config['action']['template']     = 'action.tpl.php';
$url_config['action']['controllers']  = 'action.controller.php';

$url_config['tests']['url_param']    = 'tests';
$url_config['tests']['template']     = 'tests.tpl.php';
$url_config['tests']['controllers']  = 'tests.controller.php';


$url_config['404']['url_param']    = '404';
$url_config['404']['template']     = '404.tpl.php';
$url_config['404']['controllers']  = '404.controller.php';



$GLOBALS[URL_CONFIG]       = $url_config;

define(URL_CONFIG, serialize($url_config));

?>