<title>{title}</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{description}"/>
<meta name="robots" content="all" />
<meta name="keywords" content="{keywords}" />
<link rel="canonical" href="{base_url}" type="text/html" />
<meta name="mobile-agent" content="format=html5;url=http://m.softonic.com/" />
<meta name="mobile-agent" content="format=xhtml;url=http://m.softonic.com/" />
<meta property="og:title" content="og_title" />
<meta property="og:description" content="{og_description}" />
<meta property="og:image" content="{logo}" />
<meta property="og:url" content="{base_url}" />
<meta property="og:site_name" content="{site_name}" />
<meta property="SiteAdvisor" content="For SiteAdvisor Verification Process" />
<link rel="shortcut icon" href="{favicon}" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="{bootstrapcss}" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
<link title="{SITE_NAME}" rel="search" type="application/opensearchdescription+xml" href="{BASE_URL}/opensearch.xml" />
<script type="text/javascript" src="{jquery}"></script>
<script type="text/javascript" src="{bootstrapjs}"></script>
