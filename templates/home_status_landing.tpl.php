<link rel="stylesheet" type="text/css" href="{sooncss}" media="screen" />
</head>
<body class="nomobile">

    <!-- START HEADER -->
    <section id="header">
        <div class="container">
            <header>
                <!-- HEADLINE -->
                <h1 data-animated="GoIn"><b>¿UN LUGAR?</b><br>Una nueva forma de encontarlo</h1>
            </header>
            <!-- START TIMER -->
            <div id="timer" data-animated="FadeIn">
                <p id="message"></p>
                <div id="days" class="timer_box"></div>
                <div id="hours" class="timer_box"></div>
                <div id="minutes" class="timer_box"></div>
                <div id="seconds" class="timer_box"></div>
            </div>
            <!-- END TIMER -->
     
            
        </div>
        <!-- LAYER OVER THE SLIDER TO MAKE THE WHITE TEXTE READABLE -->
        <div id="layer"></div>
        <!-- END LAYER -->
        <!-- START SLIDER -->
        <div id="slider" class="rev_slider">
            <ul>
              <li data-transition="slideleft" data-slotamount="1" data-thumb="assets/img/slider/1.jpg">
                <img src="{home_slider_1}">
              </li>
              <li data-transition="slideleft" data-slotamount="1" data-thumb="assets/img/slider/2.jpg">
                <img src="{home_slider_2}">
              </li>
              <li data-transition="slideleft" data-slotamount="1" data-thumb="assets/img/slider/3.jpg">
                <img src="{home_slider_3}">
              </li>
              <li data-transition="slideleft" data-slotamount="1" data-thumb="assets/img/slider/4.jpg">
                <img src="{home_slider_4}">
              </li>
            </ul>
        </div>
        <!-- END SLIDER -->
    </section>
    


    <script type="text/javascript" src="{modernizrjs}"></script>
    <script type="text/javascript" src="{pluginsjs}"></script>
    <script type="text/javascript" src="{themepunchjs}"></script>
    <script type="text/javascript" src="{customjs}"></script>


  </body>
</html>