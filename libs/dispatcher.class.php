<?php
class dispatcher
{
	function dispatcher($url, $url_config)
	{
		$this->url = $url;
		$this->url_config = $url_config;
		$this->run();
	}

        private function run()
	{
                $globalParams       = unserialize(SCRIPT_PARAMS);
                $ActionControllers  = unserialize(URL_CONFIG)[SCRIPT_ACTION]['controllers'];
                $ActionLibs         = unserialize(URL_CONFIG)[SCRIPT_ACTION]['libs'];
                $ActionTpl          = unserialize(URL_CONFIG)[SCRIPT_ACTION]['template'];
                $headerController   = "header.controller.php";
                //machaco todo si el estado es LANDING. 
                if(WEB_STATUS=='LANDING')
                {
                    $globalParams       = "";
                    $ActionControllers  = array("val"=>"home_status_landing.controller.php");
                    $ActionLibs         = array();
                    $ActionTpl          = "home_status_landing.tpl.php";
                    $headerController   = "header_status_landing.controller.php";
                }
               
                
                include_once(PATH_LIBS.'/class.TemplatePower.inc.php');
		include_once(PATH_LIBS.'/helper.inc.php');
		$helper = new helper();
				
		//por defecto tenemos que cargar el header
		include_once(PATH_CONTROLLER.'/'.$headerController);
			
		//cargamos el template principal
		$tpl = new TemplatePower(PATH_TEMPLATES.'/'.$ActionTpl);		
		$tpl->prepare();
		
		//asignaciones globales de todas las plantillas
		$tpl->assignGlobal("url_base", BASE_URL);
		
                //load libs and controllers
                foreach($ActionLibs as $key=>$value){
                    include_once(PATH_LIBS.'/'.$value);			
		}
                
		foreach($ActionControllers as $key=>$value){
                    include_once(PATH_CONTROLLER.'/'.$value);			
		}

		//render
		$tpl->printToScreen();

		/*
		* RENDER FOOTER
		*/
		include_once(PATH_CONTROLLER.'/footer.controller.php');
                
	}
	
	
	
}

?>