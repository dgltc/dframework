<?php
class helper
{
	function helper()
	{
		
	}
	
	function seoUrl($input)
	{
		$input = utf8_decode($input);
		/**
		 * Return URL-Friendly string slug
		 * @param string $input
		 * @return string
		*/
		$input = $this->remove_accent( $input );
		$input = str_replace("&nbsp;", " ", $input);
		$input = str_replace(array("'", "_"), "", $input); //remove single quote and dash
		$input = mb_convert_case($input, MB_CASE_LOWER, "UTF-8"); //convert to lowercase
		$input = preg_replace("#[^a-zA-Z]+#", "_", $input); //replace everything non an with dashes
		$input = preg_replace("#(-){2,}#", "$1", $input); //replace multiple dashes with one
		$input = trim($input, "_"); //trim dashes from beginning and end of string if any
		return $input;
	}
	
	function remove_accent($string)
	{
		return str_replace( array('�','�','�','�','�', '�', '�','�','�','�', '�','�','�','�', '�', '�','�','�','�','�', '�','�','�','�', '�','�', '�','�','�','�','�', '�', '�','�','�','�', '�','�','�','�', '�', '�','�','�','�','�', '�','�','�','�', '�'), array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), $string);
	}
	
	function estimate_software_quality($rating_softonic, $rating_users,  $title)
	{
		if(($rating_softonic+$rating_users)>=18) return $title. " es un excelente programa ";
		if(($rating_softonic+$rating_users)<18 AND ($rating_softonic+$rating_users)>15 ) return $title. " es un buen programa ";
		if(($rating_softonic+$rating_users)<= 15 ) return $title. " es un programa ";
		
	}
	
	function dateToString($date)
	{
		//2013-04-02
		$piezes = explode('-', $date);
		$year = $piezes[0];
		$month = $piezes[1];
		$day  = $piezes[2];
		
		$months['es'] = array(
				'01'=> 'Enero',
				'02'=> 'Febrero',
				'03'=> 'Marzo',
				'04'=> 'Abril',
				'05'=> 'Mayo',
				'06'=> 'Junio',
				'07'=> 'Julio',
				'08'=> 'Agosto',
				'09'=> 'Setiembre',
				'10'=> 'Octubre',
				'11'=> 'Noviembre',
				'12'=> 'Diciembre'
				);
		$date_string['es'] = $day*1 . " de ". $months['es'][$month] . " de ". $year;
		
		return $date_string;
	}
        

}

?>