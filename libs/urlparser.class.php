<?php
class url
{
	function url()
	{
            $this->getParams();
            $this->getGlobalParams();
	}

	private function getParams()
	{
            $params = explode('/',$_SERVER['REQUEST_URI']);
            foreach ($params as $key=>$value)
            {
		$param = explode('-', $value);
		if($param[0]=='') continue;
                $param_list[$param[0]] = $param[1];
            }
            $this->param_list = $param_list;
	}
        
                
        private function getGlobalParams()
        {
            $url_config = unserialize(URL_CONFIG);
            foreach($url_config as $key=>$value)
            {
                if(in_array($value['url_param'], array_keys($this->param_list)))
                {
                    define('SCRIPT_ACTION', $key);
                    unset($this->param_list[$value['url_param']]);
                    break;
                }
            }
            if(SCRIPT_ACTION=="SCRIPT_ACTION") define('SCRIPT_ACTION', 'home');
            
            define('SCRIPT_PARAMS',  serialize($this->param_list));
        }

}


?>