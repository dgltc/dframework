<?php

/*
 * PDO Database Class
 *	
 * @package		MYAZARC Classes
 * @category            Database Class
 * @author		myazarc
 * @require		Memcached (optional)
 * @createtime		15:40 08.04.2014 (H:i d.m.Y)[Europa/Istanbul]
 * @version		v0.5
 * @license             http://myazarc.com/myazarc-classes-license/
 * @see			http://myazarc.com/pdo-memcached-class/
 */

class db{
    #global db connect config
    var $db_type 	= 'mysql';		// database type: only use mysql,mssql,firebird,oracle
    var $db_host	= DATABASE_HOST;  	// database host or database location
    var $db_pass	= DATABASE_PASS;		// database password
    var $db_name	= DATABASE_NAME;		// database name
    var $db_port	= DATABASE_PORT;		// database port
    var $db_user        = DATABASE_USER;
    var $db_serna	= 'orcl';  		// only use oci(oracle) 
    
    var $db_cache	= FALSE;		// database for cache. only use TRUE,FALSE
    # memcache config
    var $link           = '127.0.0.1';          // memcached url
    var $port           = 11211;                // memcached port
    var $cachetime      = 10;                   // second 
    var $cachezlip      = FALSE;                // cachezlip only use bool (recommended FALSE)
    var $key_prefix     = 'myazarc';            // key prefix (optional)
    
    private $cache;                             // returning memcache connection

    private $db_conn=NULL;                   	// returning database connection

    var $is_cache=false;
    
    private $lastsql='';

    private $where='where ';
    private $countwhere=0;
    private $select='';
    private $selectcount=0;
    private $from='';
    private $fromcount=0;
    private $orderby;
    private $limit;
    private $join='';
    private $groub='';
    private $groubcount=0;
    private $orderbycount=0;


    var $rowcount=0;

    function __construct(){
        $this->connectdb();
        if($this->db_cache===TRUE){
            $this->connectmem();
        }
    }
    
    private function connectmem() {
        $this->cache=new Memcache();
	$this->cache->pconnect($this->link,$this->port);
    }
    
    private function mem_set($key,$cache){
        $key=$this->key_prefix.md5($key);
        $this->cache->set($key,$cache,$this->cachezlip,$this->cachetime);
    }

    private function mem_get($key){
        $key=$this->key_prefix.md5($key);
        return $this->cache->get($key);
    }

    public function connectdb(){

        if($this->db_type=='mysql'){
            $this->db_conn_mysql();
        }elseif($this->db_type=='mssql'){
            $this->db_conn_mssql();
        }elseif($this->db_type=='firebird'){
            $this->db_conn_firebird();
        }elseif($this->db_type=='oracle'){
            $this->db_conn_oci();
        } //if $this->db_type end

        $this->db_conn->exec('SET NAMES "utf8"');


    } //function connect end;

    private function db_conn_mysql(){
        try {
            $this->db_conn=new PDO('mysql:host='.$this->db_host.';dbname='.$this->db_name.'; port='.$this->db_port, $this->db_user, $this->db_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));      
        } catch (Exception $exc) {
            $this->show_err('Connection Error', $exc->getMessage());
            exit();
        }

    } // function db_conn_mysql end;

    private function db_conn_mssql(){
        try {
        $this->db_conn=new PDO('dblib:host='.$this->db_host.';dbname='.$this->db_name.'; port='.$this->db_port, $this->db_user, $this->db_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $exc) {
            $this->show_err('Connection Error', $exc->getMessage());
            exit();
        }
        
    } // function db_conn_mssql end;

    private function db_conn_firebird(){
        try {
            $this->db_conn=new PDO('firebird:dbname='.$this->db_name,$this->db_user, $this->db_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $exc) {
            $this->show_err('Connection Error', $exc->getMessage());
            exit();
        }
     } // function db_conn_firebird end;

    private function db_conn_oci(){
            $tns = " 
                    (DESCRIPTION =
                            (ADDRESS_LIST =
                              (ADDRESS = (PROTOCOL = TCP)(HOST = ".$this->db_host.")(PORT = ".$this->db_port."))
                            )
                            (CONNECT_DATA =
                              (SERVICE_NAME = ".$this->db_serna.")
                            )
                      )
               ";
        try{    
            $this->db_conn=new PDO("oci:dbname=".$tns,$this->db_user,$this->db_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $exc) {
            $this->show_err('Connection Error', $exc->getMessage());
            exit();
        }
    } // function db_conn_oci end;

    public function showtables(){
        $tableList = array();
        $result = $this->db_conn->query("SHOW TABLES");
        while ($row = $result->fetch(PDO::FETCH_NUM)) {
            $tableList[] = $row[0];
        }
        
        $this->lastsql='SHOW TABLES';
        $this->rowcount=$result->rowCount();
        return $tableList;
    }

    public function showtablefields($tablename,$issql=FALSE){
        $column=array();
        if($issql){
            $select = $this->db_conn->query($tablename);
        }  else {
            $select = $this->db_conn->query("SELECT * FROM $tablename LIMIT 1");
        }
        $total_column = $select->columnCount();

        for ($counter = 0; $counter < $total_column; $counter ++) {
                $meta = $select->getColumnMeta($counter);
                $column[] = $meta['name'];
        }
        $this->lastsql='SELECT * FROM $tablename LIMIT 1';
        $this->rowcount=$select->rowCount();
        return $column;
    }

    public function query($sql){
            $result="";
            $this->lastsql=$sql;
            if($this->db_cache===TRUE){

                            if(!$this->mem_get($sql)){

                            try{  
                                    $query=$this->db_conn->query($sql);
                                    if($query){
                                            foreach($query as $w){
                                                    $result[]=(object)$w;
                                            } // foreach query end;
                                            $this->mem_set($sql,$result);
                                            $this->rowcount=$query->rowCount();
                                    }else{
                                            $this->show_err($sql,$this->db_conn->errorInfo());
                                    } // if query end;
                            }catch (PDOException $e){
                                    $this->show_err($sql,$e->getMessage());
                            }//try catch end;
                                    return $result;
                            }else{
                                    $this->is_cache=TRUE;
                                    $result=$this->mem_get($sql);
                                    $this->rowcount=$query->rowCount();
                                    return $result;
                            }
            }else{

                    try{  
                            $query=$this->db_conn->query($sql);
                            if($query){
                                    foreach($query as $w){
                                            $result[]=(object)$w;
                                    } // foreach query end;
                            }else{
                                    $this->show_err($sql,$this->db_conn->errorInfo());
                            } // if query end;
                    }catch (PDOException $e){
                                    $this->show_err($sql,$e->getMessage());
                    }//try catch end;
                    $this->is_cache=FALSE;
                    $this->rowcount=$query->rowCount();
                    return $result;
                    

            }

    } // function query end;

    public function insert($tblname,$insert=array()){
            if(is_array($insert)){
                    foreach($insert as $k=>$v){
                            $key[]=$k;$val[]=$v;
                    }

                    $rowname=implode(',',$key);
                    $countkeys=count($key);
                    $rowvalue="";
                    for($i=0;$countkeys>$i;$i++){
                            $rowvalue.='?, ';
                    }

                    $rowvalue=substr($rowvalue,0,-2);

                    $sql = "INSERT INTO $tblname ($rowname) VALUES ($rowvalue)";
                    try{    
                            $q = $this->db_conn->prepare($sql);
                            if(!$q->execute($val)){
                                    $this->show_err($sql,$this->db_conn->errorInfo());
                            }	
                    }catch (PDOException $e){
                                    $this->show_err($sql,$e->getMessage());
                    } //try catch end;
                    $this->lastsql=$sql;

            }
            return $this;
    }
    
    public function delete($tblname,$where=NULL) {
        if($this->countwhere){
            $where=$this->where;
        }  else {
            $where="WHERE $where";
        }
        $sql="DELETE FROM $tblname ".$where;
        $this->db_conn->exec($sql);
        $this->lastsql=$sql;
        $this->where='where ';
        $this->countwhere=0;
        return $this;
}

    public function update($tblname,$update=array(),$where=NULL){
        $upd='';    
        if(is_array($update)){
                    foreach($update as $k=>$v){
                            $upd.="$k=?, ";$val[]=$v;
                    } //foreach update end;


                    $upd=substr($upd,0,-2);
                    $wher='';
                    if($where!==NULL){
                            $wher="where $where";
                    }

                    $sql = "UPDATE $tblname SET $upd ".($this->countwhere?$this->where:$where);
                    try{
                            $q = $this->db_conn->prepare($sql);
                            if(!$q->execute($val)){
                                    $this->show_err($sql,$this->db_conn->errorInfo());
                            }
                    }catch (PDOException $e){
                                    $this->show_err($sql,$e->getMessage());
                    } //try catch end;
            }
            $this->lastsql=$sql;
            $this->where='where ';
            $this->countwhere=0;
            return $this;
    }

    public function where($row,$val=NULL){

            if(is_array($row)){
                    foreach($row as $k=>$v){
                            if(!$this->countwhere){
                                    $this->where.="$k='$v' ";
                            }else{
                                    $this->where.="and $k='$v' ";
                            }// if countwhere end;
                        $this->countwhere++;
                    }//foreach row end;
            }else{
                    if(!$this->countwhere){
                            $this->where.="$row='$val' ";
                    }else{
                            $this->where.="and $row='$val' ";
                    }// if countwhere end;
                    $this->countwhere++;

            }// if is_array end;

            
            return $this;

    }

    public function join($tablename,$where){

            $this->join.= ' INNER JOIN '.$tablename.' ON '.$where;
            return $this;

    }

    public function join_left($tablename,$where){

            $this->join.= ' LEFT JOIN '.$tablename.' ON '.$where;
            return $this;

    }

    public function join_right($tablename,$where){

            $this->join.= ' RIGHT JOIN '.$tablename.' ON '.$where;
            return $this;

    }


    public function where_or($row,$val=NULL){

            if(is_array($row)){
                    foreach($row as $k=>$v){
                            if(!$this->countwhere){
                                    $this->where.="$k='$v' ";
                            }else{
                                    $this->where.="or $k='$v' ";
                            }// if countwhere end;
                    }//foreach row end;
            }else{
                    if(!$this->countwhere){
                            $this->where.="$row='$val' ";
                    }else{
                            $this->where.="or $row='$val' ";
                    }// if countwhere end;
            }// if is_array end;

            $this->countwhere++;
            return $this;

    }
    
    public function where_static($where){
        if(!$this->countwhere){
                $this->where.=$where." ";
        }else{
                $this->where.="and $where ";
        }// if countwhere end;
        
        $this->countwhere++;
        return $this;
    }

    public function where_in($row,$val=NULL,$clause='and'){

            if(is_array($row)){
                    foreach($row as $k=>$v){
                            if(!$this->countwhere){
                                    $this->where.="$k in ($v) ";
                            }else{
                                    $this->where.="$clause $k in ($v) ";
                            }// if countwhere end;
                    }//foreach row end;
            }else{
                    if(!$this->countwhere){
                            $this->where.="$row in ($val) ";
                    }else{
                            $this->where.="$clause $row in ($val) ";
                    }// if countwhere end;
            }// if is_array end;

            $this->countwhere++;
            return $this;

    }

    public function where_like($row,$val=NULL,$clause='and'){

            if(is_array($row)){
                    foreach($row as $k=>$v){
                            if(!$this->countwhere){
                                    $this->where.="$k like '%$v%' ";
                            }else{
                                    $this->where.="$clause $k like '%$v%' ";
                            }// if countwhere end;
                    }//foreach row end;
            }else{
                    if(!$this->countwhere){
                            $this->where.="$row like '%$val%' ";
                    }else{
                            $this->where.="$clause $row like '%$val%' ";
                    }// if countwhere end;
            }// if is_array end;

            $this->countwhere++;
            return $this;
    }

    public function where_between($row,$val1=NULL,$val2=NULL,$clause='and'){
            if(!$this->countwhere){
                    $this->where.="$row between $val1 and $val2 ";
            }else{
                    $this->where.="$clause $row between $val1 and $val2 ";
            }// if countwhere end;

            $this->countwhere++;
            return $this;
    }


    public function from($from){
            if($this->fromcount>0){
                    $this->from.=','.$from;
            }else{
                    $this->from.=$from;
            }

            $this->fromcount++;
            return $this;
    }

    public function select($select){
            if($this->selectcount>0){
                    $this->select.=','.$select;
            }else{
                    $this->select.=$select;
            }

            $this->selectcount++;
            return $this;
    }

    public function groupby($groub){
            if($this->groubcount>0){
                    $this->groub.=','.$groub;
            }else{
                    $this->groub.='GROUP BY '.$groub;
            }

            $this->groubcount++;
            return $this;
    }

    public function get($tblname=NULL){
            if($tblname!==NULL){
                    $this->from=$tblname;
            }

            if($this->select=='') $this->select='*';

            $sql='select '.$this->select.' from '.$this->from.' '.$this->join.' '.($this->countwhere?$this->where:' ').$this->orderby.$this->limit.' '.$this->groub;

            $this->where='where ';
            $this->countwhere=0;
            $this->groub='';
            $this->join='';
            $this->groubcount=0;
            $this->select='';
            $this->selectcount=0;
            $this->from='';
            $this->fromcount=0;
            $this->orderbycount=0;
            $this->orderby='';
            $this->limit='';
            
          
            return $this->query($sql);

    }

    public function get_sql($clearSql=FALSE){
        
        if($clearSql){
            $this->where='where ';
            $this->countwhere=0;
            $this->groub='';
            $this->join='';
            $this->groubcount=0;
            $this->select='';
            $this->selectcount=0;
            $this->from='';
            $this->fromcount=0;
            $this->orderbycount=0;

        }

        return $this->lastsql;

    }

    public function orderby($orderby,$type='asc'){
        if($this->orderbycount>0){
                $this->orderby.=','.$orderby.' '.$type;
        }else{
                $this->orderby.='order by '.$orderby.' '.$type;
        }
        $this->orderbycount++;
        return $this;
    }

    public function limit($limit,$limit2=NULL){
        if($limit2!==NULL){
            $this->limit="limit ".$limit.','.$limit2;
        }else{
            $this->limit="limit ".$limit;
        }
        return $this;
    }
    
    public function show_err($sql,$sqlerr){
        echo '<div><strong>SQL:</strong> '.$sql.'<br>';
        echo '<strong>SQL Error:</strong> '.$sqlerr.'<div>';
        return FALSE;
    }
            
    public function rowcount() {
        return $this->rowcount;
    }
    
    public function debug() {
        header('Content-Type: text/html; charset=utf-8');
        $sql=$this->lastsql;
        $colname=$this->showtablefields($sql,TRUE);
        $result=$this->query($sql);
        echo '<strong> Last Sql: </strong>'.$sql.'<br><br>';
        echo '<table cellspacing=0 border=1 style="font-size:9px;"><tr>';
        echo '<th style="padding:3px; border:1px solid #333;border-collapse:collapse; background-color:#ccc">Row No</th>';
        $i=1;
        foreach($colname as $q){
            echo '<th style="padding:3px; border:1px solid #333;border-collapse:collapse; background-color:#ccc">'.$q.'</th>';
         } 
        echo '</tr>';
        foreach($result as $q){
        echo '<tr>';
        echo '<td style="border:1px solid #333;">&nbsp;'.$i.'</td>';
            foreach($colname as $w){ 
                echo '<td style="border:1px solid #333;">&nbsp;'.$q->$w.'</td>';
            }
        echo '</tr>';
        $i++;
        }
        echo '</table>';
    }
    
    function __desctruct(){
        $this->db_conn=NULL;
    } // function __desctruct end;
}

$db = new db();