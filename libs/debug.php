
<div class="debug">
<?php
$globalParams       = unserialize(SCRIPT_PARAMS);
$ActionControllers  = unserialize(URL_CONFIG)[SCRIPT_ACTION]['controllers'];
$ActionLibs         = unserialize(URL_CONFIG)[SCRIPT_ACTION]['libs'];
$ActionTpl          = unserialize(URL_CONFIG)[SCRIPT_ACTION]['template'];

echo "<pre>";
echo "Parametros globales <br>";
print_r($globalParams);
echo "<hr>";
echo "Controladores necesarios <br>";
print_r($ActionControllers);
echo "<hr>";

echo "Librerias necesarias <br>";
print_r($ActionLibs);
echo "<hr>";

echo "Plantilla principal <br>";
print_r($ActionTpl);
echo "<hr>";

echo "</pre>";
?>
</div>


