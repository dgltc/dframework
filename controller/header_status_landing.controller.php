<?php
$header = new TemplatePower(PATH_TEMPLATES.'/header_status_landing.tpl.php');		
$header->prepare();
$header->assign('bootstrapcss',     PATH_CSS.'/bootstrap.min.css');
$header->assign('sooncss',          PATH_CSS.'/soon.css');

$header->assign('jquery',           PATH_JS.'/jquery.min.js');
$header->assign('bootstrapjs',      PATH_JS.'/bootstrap.js');


$header->assign('title', "site title");

$header->printToScreen();
?>
