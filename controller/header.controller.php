<?php
error_reporting(E_ALL);
$header = new TemplatePower(PATH_TEMPLATES.'/header.tpl.php');		
$header->prepare();
$header->assign('js',               PATH_JS.'/bootstrap.min.js');
$header->assign('bootstrapcss',     PATH_CSS.'/bootstrap.min.css');
$header->assign('themecss',         PATH_CSS.'/bootstrap-theme.min.css');
$header->assign('spain_js',         PATH_JS.'/spain.js');
$header->assign('knob',             PATH_JS.'/jquery.knob.min.js');
$header->assign('item_box_js',      PATH_JS.'/item_box.js');
$header->assign('item_box_css',     PATH_CSS.'/item_box.css');


$header->assign('title', "site title");

$header->printToScreen();
?>
